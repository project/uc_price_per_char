Contents:

 * Introduction
 * Requirements
 * Installation
 * Global configuration
 * Configuring a product
 * Contact



Introduction:
=============

UC Price per char adds a simple feature to Ubercart products allowing the
price to be adjusted based on the length of a text field attribute.


Requirements:
=============

Ubercart: Store, Cart, Product and Product attributes.


Installation:
=============

As per usual for contributed modules, see: 
https://drupal.org/documentation/install/modules-themes/modules-7


Global configuration:
=====================

Global settings are located at: 
Administration > Store > Configuration > Products > Price per char

Strip spaces:
  Sets whether spaces should count towards the string length or not.

Hide existing price display:
  Always: Prices are always hidden, only price per char is shown.
  Never: Prices are never hidden, price per char is shown as additional.
  Only when price = 0: Price is hidden if it equals 0.

Price per char title:
  Prefix title to show with price tag.

Alternate title:
  Alternate title to use if base price is hidden for any of the reasons
  as configured by the "Hide existing price display" setting.


Configuring a product:
======================

First the product should have a text field type attribute, see the relevant
Ubercart product attributes documentation if you need more info on this.

On the product 'edit' page, select "features", choose "Price per char" from
the dropdown list and click 'Add'.

On the following page, enter the appropriate price per character and select
the relevant attribute from the dropdown list. Choose 'Save feature'.

That's all. :)


Contact:
========

Current maintainer: Daniel Pekelharing:  http://drupal.org/user/94275
